
from monitor.models import *
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets, generics, response, status
from rest_framework.decorators import *
from rest_framework.permissions import IsAuthenticatedOrReadOnly
import json

from datetime import timedelta
from django.utils import timezone

def delete_old(model, time_field, **kwargs):

    filter_args = {time_field+"__lt":timezone.now()-timedelta(**kwargs)}

    objects = model.objects.filter(**filter_args).delete()

def API_request(model, time_field, request):

    tail = -1

    if 'latest' in request.GET:
        tail = 1
    if 'tail' in request.GET:
        tail = int(request.GET['tail'])

    filter_args = {time_field+"__gte":timezone.now()-timedelta(days=2)}

    objects = model.objects.filter(**filter_args).order_by(time_field)

    # get the last Nth objects
    if tail > -1:
        l = len(objects)
        if l > 0 and l >= tail:
            objects = objects[l - tail:]
    else:
        names = {} # assemble the filter
        if request.GET is not None:
            for item in request.GET:
                print(item)
                names[item] = request.GET[item]
            #print(request.GET,request.GET.getlist(item),names[item])
            objects = objects.filter(**names)
    #         print(objects)
    # print(objects)
    return objects


#
# Serializers!
#

# Serializers define the API representation.
class User_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')

class TelescopeStatusInformation_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TelescopeStatusInformation
        fields = ('dome_az', 'timestamp', 'tel_ra', 'tel_dec', 'last_issued_command', 'tel_az', 'tel_alt', 'tel_status')

# Serializers define the API representation.
class TelescopeLog_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TelescopeLog
        fields = ('priority', 'message', 'timestamp', 'date_added')





# Serializers define the API representation.
class WeatherStatusInformation_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WeatherStatusInformation
        fields = ('current_temp', 'humidity', 'dewpoint', 'wind_speed', 'wind_direction', 'local_time', 'press_atm')

# Serializers define the API representation.
class AllSkyImage_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AllSkyImage
        fields = ('image_link', 'image_name', 'date_time_added', 'date_time_taken')

class TelescopeImage_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TelescopeImage
        fields = ('image_link', 'image_name', 'date_time_added', 'date_time_taken')

# Serializers define the API representation.
class TargetInformation_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TargetInformation
        fields = ('name', 'target_ra', 'target_dec', 'scheduled_at', 'scheduled_for', 'timestamp', 'scheduled_by', 'last_update', 'status')

# Serializers define the API representation.
class TargetList_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TargetList
        fields = ('target_list','timestamp')

class SiteStatus_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SiteStatus
        fields = ('status', 'timestamp')


#
# Viewsets!
#

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = User_Serializer

class TelescopeStatusViewSet(viewsets.ModelViewSet):
    queryset = TelescopeStatusInformation.objects.all()
    serializer_class = TelescopeStatusInformation_Serializer

    def list(self, request):

        objects = API_request(TelescopeStatusInformation, "timestamp", request)

        delete_old(TelescopeStatusInformation, "timestamp", days=2)

        return response.Response(self.serializer_class(objects, many=True).data)

class TelescopeLogViewSet(viewsets.ModelViewSet):
    queryset = TelescopeLog.objects.all()
    serializer_class = TelescopeLog_Serializer

    def list(self, request):
        objects = API_request(TelescopeLog, "date_added", request)

        return response.Response(self.serializer_class(self.queryset, many=True).data)


    def create(self, request):

        parsed_log = []
        field_list=['timestamp','priority','message']

        print('create was called')

        delete_old(TelescopeLog, "date_added", hours=1)

        if 'file' in request.data:

            tmpfile = request.FILES['file'].read()
            #print(tmpfile)
            for line in tmpfile.decode('utf-8').splitlines():
                #print(line)
                if line != '\n' and len(line) >0:
                    tmp = [x.strip('[') for x in line.split(']',2)]
                    tmp[-1] = tmp[-1].strip()

                    tmp[0] = tmp[0].replace(" ",'T').split(',')[0]
                    #print(tmp)
                    tmp = dict(zip(field_list,tmp))

                    parsed_log.append(tmp)

            serializer = TelescopeLog_Serializer(data=parsed_log, many=True)

            if serializer.is_valid():

                serializer.save()

                print("successfully saved log")

                return response.Response(status=status.HTTP_202_ACCEPTED)

            else:
                return response.Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

        else:
            return response.Response({'error': 'i sent this error'},status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['delete'])
    def delete(self, request):

        self.queryset.delete()

        return response.Response(status=status.HTTP_202_ACCEPTED)


# ViewSets define the view behavior.
class WeatherStatusView(viewsets.ModelViewSet):
    queryset = WeatherStatusInformation.objects.all()
    serializer_class = WeatherStatusInformation_Serializer

    def list(self, request):

        objects = API_request(WeatherStatusInformation, "local_time", request)

        delete_old(WeatherStatusInformation, "local_time", days=2)

        return response.Response(self.serializer_class(objects, many=True).data)


# ViewSets define the view behavior.
class AllSkyImageViewSet(viewsets.ModelViewSet):
    queryset = AllSkyImage.objects.all()
    serializer_class = AllSkyImage_Serializer

    def list(self, request):

        objects = API_request(AllSkyImage, "date_time_added", request)

        delete_old(AllSkyImage, "date_time_added", days=2)

        return response.Response(self.serializer_class(objects, many=True).data)

class TelescopeImageViewSet(viewsets.ModelViewSet):
    queryset = TelescopeImage.objects.all()
    serializer_class = TelescopeImage_Serializer

    def list(self, request):

        objects = API_request(TelescopeImage, "date_time_added", request)

        delete_old(TelescopeImage, "date_time_added", days=2)

        return response.Response(self.serializer_class(objects, many=True).data)

# ViewSets define the view behavior.
class TargetInformationViewSet(viewsets.ModelViewSet):
    queryset = TargetInformation.objects.all()
    serializer_class = TargetInformation_Serializer

    def list(self, request):

        objects = API_request(TargetInformation, "timestamp", request)

        delete_old(TargetInformation, "timestamp", days=2)

        return response.Response(self.serializer_class(objects, many=True).data)

class StatusViewSet(viewsets.ModelViewSet):
    queryset = SiteStatus.objects.all()
    serializer_class = SiteStatus_Serializer

    def list(self, request):

        objects = API_request(SiteStatus, "timestamp", request)

        delete_old(SiteStatus, "timestamp", days=2)

        return response.Response(self.serializer_class(objects, many=True).data)

class TargetListViewSet(viewsets.ModelViewSet):
    #permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = TargetList.objects.all()
    serializer_class= TargetList_Serializer

    def list(self, request):
        #print(request.user)

        objects = TargetList.objects.latest('timestamp')
    #     print(self.queryset)
    #     # objects = TargetInformation.objects.filter(name_in=)
    #

        #print(objects.target_list)
        target_models_list = []
        for target in json.loads(objects.target_list):
            target_models_list.append(TargetInformation.objects.filter(name=target)[0])
        #print(target_models_list)
        return response.Response(TargetInformation_Serializer(target_models_list,many=True).data)
