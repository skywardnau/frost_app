
// the timing for how often ajax should query the database
var ajax_delay = 5 * 1000; // in milliseconds

// this is when the page is fully loaded
$(document).ready(function() {

    log_buttons();
    update_logs();

    update_telescope();

    var oldDate;

    // populate weather objects upon load
    $.get(base_url + '/jackfrost/weather', function(response) {
        for (var e in response) {
            var r = response[e];
            addWeatherData(r.current_temp, r.humidity, r.wind_speed, r.wind_direction, r.dewpoint, new Date(r.local_time));
        }
        oldDate = date[date.length - 1];
        updateCharts();
    });

    // populate target list upon load
    $.get(base_url + '/jackfrost/targets?tail=10', function(response) {

        var panelList = $('#PanelList');

        for (var e in response) {

            // build the row from html
            var r = response[e];
            var out = "<tr>"
            out += "    <td>" + r.name + "</td>";
            out += "    <td>" + r.target_ra + "</td>";
            out += "    <td>" + r.target_dec + "</td>";
            out += "    <td>" + r.status + "</td>";
            out += "    <td>" + r.scheduled_for + "</td>";
            out += "</tr>"

            // put it out into the table
            panelList.append(out);
        }
    });






    // get new weather every few seconds
    // and populate weather objects with new data
    window.setInterval(function() {

        // telescope
        update_telescope();



        // weather
        $.get(base_url + '/jackfrost/weather/?latest', function(content) {

            //console.log('latest content:', content);

            var latestWeatherObject = content[0];

            // grab our datetimefield from the model and convert it to look a bit nicer
            var newDate = new Date(latestWeatherObject.local_time);

            if (oldDate.getTime() != newDate.getTime()) {
                //console.log(oldDate, date);
                addWeatherData(
                    latestWeatherObject.current_temp,
                    latestWeatherObject.humidity,
                    latestWeatherObject.wind_speed,
                    latestWeatherObject.wind_direction,
                    latestWeatherObject.dewpoint,
                    newDate
                );
                popWeatherData();
                updateCharts();
            }
            oldDate = newDate;
        });

        // update target list every 60 seconds load
        $.get(base_url + '/jackfrost/targets?tail=10', function(response) {

            var panelList = $('#PanelList');
            panelList.empty();

            for (var e in response) { //<li class='panel panel-info'>
                var r = response[e];
                var out = "<tr>"
                out += "    <td>" + r.name + "</td>";
                out += "    <td>" + r.target_ra + "</td>";
                out += "    <td>" + r.target_dec + "</td>";
                out += "    <td>" + r.status + "</td>";
                out += "    <td>" + r.scheduled_for + "</td>";
                out += "</tr>"
                panelList.append(out);
            }
        });

    }, ajax_delay);

    window.setInterval(function(){

        update_logs();
    }, ajax_delay*72);

}); // $(document).ready(function() {
