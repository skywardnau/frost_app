var horizonalLinePlugin = {
  afterDraw: function(chartInstance) {
    var yValue;
    var yScale = chartInstance.scales["y-axis-0"];
    var xScale = chartInstance.scales["x-axis-0"];
    var canvas = chartInstance.chart;
    var ctx = canvas.ctx;
    var index;
    var line;
    var style;
    var dash;

    if (chartInstance.options.horizontalLine) {
      for (index = 0; index < chartInstance.options.horizontalLine.length; index++) {
        line = chartInstance.options.horizontalLine[index];

        if (!line.style) {
          style = "rgba(169,169,169, .6)"; // default color for the line
        } else {
          style = line.style;
        }

        if (line.y) {
          yValue = yScale.getPixelForValue(line.y); // yValue is the pixel row on the canvas that corresponds to the certain y value on the graph
        } else {
          yValue = 0;
        }

        if(line.dash)
            dash = line.dash;
        else
            dash = [];

        ctx.lineWidth = 3;

        // draws the line straight across the graph at the given y value
        if (yValue && xScale.maxIndex > 0) {
          ctx.beginPath();
          ctx.moveTo(xScale.getPixelForTick(0), yValue);
          ctx.setLineDash(dash);
          ctx.lineTo(xScale.getPixelForTick(xScale.maxIndex), yValue);
          ctx.strokeStyle = style;
          ctx.stroke();
          ctx.setLineDash([]);
        }

        if (line.text) {
          ctx.fillStyle = style;
          ctx.fillText(line.text, 0, yValue + ctx.lineWidth);
        }
      }
      return;
    }
  }
};
Chart.pluginService.register(horizonalLinePlugin);


// Create the Charts

// the canvas for the chart
var ctxTEMP = document.getElementById("tempChart");

// this creates the chart using the canvas above
var tempChart = new Chart(ctxTEMP, {
    type: 'line',
    data: {
        labels: dateLabels,
        datasets: [{
            label: 'Temperature',
            data: TempData,
            backgroundColor: "rgba(100,255,100,0.1)",
            borderWidth: 4,
            borderColor: "rgba(100,230,100,1)"
        }]
    },
    options: {
        horizontalLine: [{ // triggers the horizontal dashed line to be drawn
            // this uses the horizonalLinePlugin defined above
              y: 90,
              style: "rgba(255, 0, 0, .4)",
              dash : [5,5],
              // text: "THRESHOLD"
            }],
        legend: {
            display: true
        },
        animation: false, // don't make this true, it breaks the graph
        scales: {
            yAxes: [{
                // left y-axis
                position: "left",
                scaleLabel: {
                    display: true,
                    labelString: 'Degrees Celsius'
                },
                ticks: {
                    beginAtZero: true,
                    max: 100,
                    stepSize: 20
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: false,
                    labelString: 'Temperature Data Over Last 48 Hours'
                }
            }]
        }
    }
});


// the canvas for the chart
var ctxDEW = document.getElementById("dewChart");

// this creates the chart using the canvas above
var dewChart = new Chart(ctxDEW, {
    type: 'line',
    data: {
        labels: dateLabels,
        datasets: [ {
            label: 'Dew Point',
            data: DewData,
            backgroundColor: "rgba(100,100,255,0.1)",
            borderWidth: 4,
            borderColor: "rgba(100,100,230,1)"
        }]
    },
    options: {
        horizontalLine: [{ // triggers the horizontal dashed line to be drawn
              y: 90,
              style: "rgba(255, 0, 0, .4)",
              dash : [5,5]
            }],
        legend: {
            display: true
        },
        animation: false, // don't make this true, it breaks the graph
        scales: {
            yAxes: [{
                // left y-axis
                position: "left",
                scaleLabel: {
                    display: true,
                    labelString: 'Degrees Celsius'
                },
                ticks: {
                    beginAtZero: true,
                    max: 100,
                    stepSize: 20
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: false,
                    labelString: 'Dew Point Data Over Last 48 Hours'
                }
            }]
        }
    }
});


// the canvas for the chart
var ctxHUM = document.getElementById("humChart");

// this creates the chart using the canvas above
var humChart = new Chart(ctxHUM, {
    type: 'line',
    data: {
        labels: dateLabels,
        datasets: [{
            label: 'Humidity',
            data: HumidData,
            backgroundColor: "rgba(255,100,100,0.1)",
            borderWidth: 4,
            borderColor: "rgba(230,100,100,1)"
        }]
    },
    options: {
        horizontalLine: [{ // triggers the horizontal dashed line to be drawn
              y: 90,
              style: "rgba(255, 0, 0, .4)",
              dash : [5,5]
            }],
        legend: {
            display: true
        },
        animation: false, // don't make this true, it breaks the graph
        scales: {
            yAxes: [{
                // left y-axis
                position: "left",
                scaleLabel: {
                    display: true,
                    labelString: '% Humidity'
                },
                ticks: {
                    beginAtZero: true,
                    max: 100,
                    stepSize: 20
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: false,
                    labelString: 'Humidity Data Over Last 48 Hours'
                }
            }]
        }
    }
});
