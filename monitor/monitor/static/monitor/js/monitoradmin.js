var panelList = $('#PanelList');
var submit_list = [];



// do some AJAX to populate the list

$(document).ready(function() {
    var base_url = "/" + (location.hostname == "localhost" ? "" : "frost");

      var get_list = function(){ $.get(base_url + '/jackfrost/targets?tail=10', function(response){

            for(var e in response){
                var r = response[e];
                var out = "<tr class='panel'>"
                    out += "    <td id='name'>" + r.name + "</td>";
                    out += "    <td id='target_ra'>" + r.target_ra + "</td>";
                    out += "    <td id='target_dec'>" + r.target_dec + "</td>";
                    out += "    <td id='target_status'>" + r.status + "</td>";
                    out += "    <td id='target_scheduled'>" + r.scheduled_for + " <input id='targetDel' class='delete_button' type='button' value='X'/></td>";
                    //<input id='targetDel' class='delete_button' type='button' value='X'/>
                    out += "</tr>"
                panelList.append(out);
            }
        });
      };

    get_list();
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();

    //button acctions
    $('#PanelList').on('click','input',function(){

      console.log($(this).closest('tr'));
      console.log("delete got clicked");


      $(this).closest('tr').remove();

    });

    $('#refresh_list').click(function(){

      console.log("refresh was clicked");
      panelList.empty();
      get_list();

    });

    $('#submit_new_list').click(function(){
      submit_list = [];
      console.log("submit was clicked");
      $('.panel', panelList).each(function(index, elem) {

          var target_name = $('#name', elem)[0].innerHTML;
          console.log("name:", target_name);

          submit_list.push(target_name);


      });

      var user = '{{user.get_username}}';
      var sl_json_str = JSON.stringify(submit_list);
      $.post('/frost/jackfrost/targetlist/',{csrfmiddlewaretoken:csrftoken,target_list:sl_json_str});

    });

});



panelList.sortable({
    // Only make the .panel-heading child elements support dragging.
    // Omit this to make then entire <li>...</li> draggable.
    //handle: '.panel-heading',
    update: function() {
        $("#out").text("");
        $('.panel', panelList).each(function(index, elem) {
            var $listItem = $(elem),
                 newIndex = $listItem.index();

            var content = $listItem.text();
            //console.log("content:", content);
        });
    }
});

function alertAdmin(){
  var response = confirm("Telescope-Shutdown initiated. Are you sure you want Shutdown?");
  if (response == true) {
      $("#shutdown_button_form").css("background-color", "red");
      $("#shutdown_button").css("display", "none");
      $("#reset_shutdown_button").css("display", "inline");

      // send some ajax to jackfrost letting it know to change the shutdown flag
  } else {
      // do nothing - cancel shutdown request
  }
}

function resetAdmin(){
  var response = confirm("Reset-Telescope initiated. Are you sure you want Reset?");
  if (response == true) {
      $("#shutdown_button_form").css("background-color", "white");
      $("#shutdown_button").css("display", "inline");
      $("#reset_shutdown_button").css("display", "none");


      // send some ajax to jackfrost letting it know to change the shutdown flag
  } else {
      // do nothing - cancel shutdown request
  }
};
