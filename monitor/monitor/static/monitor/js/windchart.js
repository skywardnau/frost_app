
    var ctx = document.getElementById("windChart");

    function create_wind_datasets() {
        // all the data points will belong to their own data set
        // this is necessary to put multiple points along the same direction
        var wind_sets = [];
        for (var i = 0; i < WSpeedData.length; i++) {
            var ws = WSpeedData[i];
            var wd = WDirData[i];
            var data_index = deg_to_index(wd);

            var radii = []; // since we want the radius of only one point in the data set to be non-zero, we use an array
            var set = {
                label: 'speed (mph)',
                data: [],
                pointBackgroundColor: [],

                pointRadius: radii,
                pointHoverRadius: radii,
                radius: radii,

                // No background or border here!
                pointBorderColor: "rgba(0,0,0,0)",
                borderWidth: 0,
                backgroundColor: "rgba(0,0,0,0)",
                borderColor: "rgba(0,0,0,0)"
            };

            // initialize all the data in the data set with zero
            // and make sure they are hidden
            for (var k = 0; k < 12; k++) {
                set.data[k] = 0;
                radii[k] = 1;
                set.pointBackgroundColor[k] = "rgba(100,10,255,0)";
            }

            // if this point is the most current point, make it look different
            if (i + 1 == WSpeedData.length) {
                radii[data_index] = 12; // bigger
                set.pointBackgroundColor[data_index] = "rgba(100,10,255,1)"; // BRIGHTER!
            } else {
                // this is an old point, make it fade
                radii[data_index] = 10 * i / WSpeedData.length;
                set.pointBackgroundColor[data_index] = "rgba(100,10,255,0.4)";
            }

            // put the data point inside the data set now
            set.data[data_index] = ws;
            wind_sets.push(set);
        }
        return wind_sets;
    }



    var windChart = new Chart(ctx, {
        type: 'radar',
        data: {
            labels: direction_labels,
            datasets: create_wind_datasets()
        },
        options: {
            legend: {
                display: false
            },
            animation: false, // don't make this true, it breaks the graph
            scale: {
                ticks: {
                    display: true,
                    min: 0,
                    max: 40,
                    stepSize: 4
                }
            }
        }
    });


        var oldDate = '';

        function addWeatherData(newTemp, newHum, newWSpeed, newWDir, newDew, newDate) {
            TempData.push(newTemp);
            HumidData.push(newHum);
            WSpeedData.push(newWSpeed);
            WDirData.push(newWDir);
            DewData.push(newDew);

            var options = {
                year: 'numeric',
                month: 'long',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                second: 'numeric'
            };
            //dateLabels.push(newDate.toLocaleDateString('en-US', options));
            dateLabels.push("");

            date.push(newDate);

            thresholdpoints.push(90);
        }

        function popWeatherData() {
            TempData.shift();
            HumidData.shift();
            WSpeedData.shift();
            WDirData.shift();
            DewData.shift();
            date.shift();
            dateLabels.shift();

            thresholdpoints.shift();
        }

        function updateCharts() {

            // this completely contructs the data sets from scratch
            windChart.data.datasets = create_wind_datasets();

            tempChart.update();
            dewChart.update();
            humChart.update();
            windChart.update();

            var i = TempData.length - 1;

            $("#current_temp").html(TempData[i] + " C");
            $("#current_humidity").html(HumidData[i] + " %");
            $("#current_dewpoint").html(DewData[i]);
            $("#wind_speed").html(WSpeedData[i] + " MPH");
            $("#wind_direction").html(direction_labels[deg_to_index(WDirData[i])]);

            $("#time_created").html(dateLabels[i]);
        }
