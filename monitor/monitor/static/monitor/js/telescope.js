var update_telescope = function(){
  // go get the response from the API

  // send the query
  $.get(base_url + '/jackfrost/telescope/?latest', function(response) {

      var tsi = response[0];
      // tsi is the first object in the response list

      var prettyDate = new Date(tsi.timestamp);

      // make the timestamp look better
      var current_timestamp = $("#timestamp").text();

      // if this is a new date:
      if(current_timestamp != prettyDate)
      {
          if(current_timestamp != '')
                $("#telescope-status").children().animate({backgroundColor:"#286090",color:"white"}, 250).animate({backgroundColor:"white",color:"black"}, 250);

            $("#dome_az").text(tsi.dome_az);
            $("#timestamp").text(prettyDate);
            $("#last_issued_command").text(tsi.last_issued_command);
            $("#tel_ra").text(tsi.tel_ra);
            $("#tel_dec").text(tsi.tel_dec);
            $("#tel_az").text(tsi.tel_az);
            $("#tel_alt").text(tsi.tel_alt);
            $("#tel_status").text(tsi.tel_status);
            //console.log("update telescope");
      }

  });

};
