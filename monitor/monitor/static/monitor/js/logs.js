var update_logs = function(){

  $.get(base_url + '/jackfrost/log/?latest', function(response) {

      var logtable = $("#logtablebody");
      logtable.children("#ltplaceholder").hide();
      for(var e in response){

        var r = response[e];
        var prettyDate = new Date(r.timestamp);

        var newrow = "<tr class=logrow>"
            newrow+= "<th id=timestamp scope=row>"+prettyDate+"</th>"
            newrow+= "<td id=logname>"+r.priority+"</td>"
            newrow+= "<td id=message>"+r.message +"</td>"
            newrow+= "</tr>"

        logtable.append(newrow);

      };


});
};

var log_buttons = function(){
$("#debug").click(function(){
  console.log("#debug got clicked");
$('#logtablebody').children('.logrow').each(function(e) {
		$(this).show();

  	var name = $(this).children("#logname").text();

    if (name != "DEBUG") {
    	$(this).toggle();
    };

});
$("#logtablebody").children("#ltplaceholder").hide();
});
$("#info").click(function(){
    console.log("#info got clicked");
$('#logtablebody').children('.logrow').each(function(e) {

    $(this).show();
  	var name = $(this).children("#logname").text();

    if (name != "INFO") {
    	$(this).toggle();
    };


});
$("#logtablebody").children("#ltplaceholder").hide();
});
$("#critical").click(function(){
    console.log("#critical got clicked");
$('#logtablebody').children('.logrow').each(function(e) {
		$(this).show();
  	var name = $(this).children("#logname").text();

    if (name != "CRITICAL") {
    	$(this).toggle();
    };

});
$("#logtablebody").children("#ltplaceholder").hide();
});
$("#all").click(function(){
  console.log("#all got clicked");
$('#logtablebody').children('.logrow').each(function(e) {
  $(this).children('#logname:hidden').each(function() {

    	$(this).parent().toggle();

  });

});
$("#logtablebody").children("#ltplaceholder").hide();
});
};
