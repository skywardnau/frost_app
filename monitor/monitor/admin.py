from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(TelescopeStatusInformation)
admin.site.register(WeatherStatusInformation)
admin.site.register(TelescopeLog)
admin.site.register(AllSkyImage)
admin.site.register(TargetInformation)
