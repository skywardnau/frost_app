from __future__ import unicode_literals

from django.db import models

# Create your models here.
class TelescopeStatusInformation(models.Model):
    dome_az = models.DecimalField(max_digits=10,decimal_places=2)
    timestamp = models.DateTimeField()
    last_issued_command = models.CharField(max_length=64)
    tel_ra = models.DecimalField(max_digits=10,decimal_places=2)
    tel_dec = models.DecimalField(max_digits=10,decimal_places=2)
    tel_az = models.DecimalField(max_digits=10,decimal_places=2)
    tel_alt = models.DecimalField(max_digits=10,decimal_places=2)
    tel_status = models.CharField(max_length=64)

class TelescopeLog(models.Model):

    priority = models.CharField(max_length=20)
    message = models.CharField(max_length=300)
    timestamp = models.DateTimeField()
    date_added = models.DateField(auto_now_add = True)

class WeatherStatusInformation(models.Model):
    current_temp = models.DecimalField(max_digits=10,decimal_places=2)
    humidity = models.DecimalField(max_digits=10,decimal_places=2)
    dewpoint = models.DecimalField(max_digits=10,decimal_places=2)
    wind_speed = models.DecimalField(max_digits=10,decimal_places=2)
    wind_direction = models.DecimalField(max_digits=10,decimal_places=2)
    press_atm = models.DecimalField(max_digits=10,decimal_places=2)
    local_time = models.DateTimeField(auto_now_add = True)

class AllSkyImage(models.Model):
    image_link = models.CharField(max_length=256)
    image_name = models.CharField(max_length=64)
    date_time_taken = models.DateTimeField()
    date_time_added = models.DateTimeField(auto_now_add = True)

class TargetInformation(models.Model):
    statusScheduled = "scheduled"
    statusObserved = "observed"
    statusDiscarded = "discarded"

    name = models.CharField(max_length=64)
    target_ra = models.DecimalField(max_digits=10, decimal_places=6)
    target_dec = models.DecimalField(max_digits=10, decimal_places=6)
    scheduled_at = models.DateTimeField() # start timestamp
    scheduled_for = models.DateTimeField() # end timestamp
    last_update = models.DateTimeField()
    scheduled_by = models.CharField(max_length=64, default="")
    timestamp = models.DateTimeField(auto_now_add = True)
    status = models.CharField(max_length=20, choices=(
        (statusScheduled, statusScheduled),
        (statusObserved, statusObserved),
        (statusDiscarded, statusDiscarded)
        ),
        default = statusScheduled)

class TelescopeImage(models.Model):
    image_link = models.URLField()
    image_name = models.CharField(max_length=256)
    date_time_added = models.DateTimeField()
    date_time_taken = models.DateTimeField()

class SiteStatus(models.Model):
    status = models.CharField(max_length=64)
    timestamp = models.DateTimeField(auto_now_add = True)

class TargetList(models.Model):
    target_list = models.TextField()
    timestamp = models.DateTimeField(auto_now_add = True)
