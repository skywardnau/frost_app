from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import authenticate, login
from django.http import JsonResponse
from datetime import datetime
from datetime import timedelta
from django.utils import timezone

from rest_framework.response import Response
from monitor.models import *
from monitor.serializers import *

def index(request):
    # weather_objects = WeatherStatusInformation.objects.latest('local_time')
    # telescope_objects = TelescopeStatusInformation.objects.latest('local_time')
    # weather_list = WeatherStatusInformation.objects.filter(local_time__gte=timezone.now()-timedelta(days=2)).order_by('local_time')
    #
    #
    # context = {
    #     'weather_list': weather_list,
    #     'weather_objects': weather_objects,
    #     'telescope_objects': telescope_objects,
    # }
    # #output = "Current Temperature:  ".join([q.current_temp for q in current_temp_obj])
    return render(request, 'monitor/index.html')#, context)

def login_admin(request):
    return render(request, 'monitor/login.html')


def admin(request):
    username = request.POST.get('username', 'username')
    password = request.POST.get('password', 'password')
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        # Redirect to a success page.
        return render(request, 'monitor/admin.html')

    else:
        # Return an 'invalid login' error message.
        return render(request, 'monitor/login.html')
