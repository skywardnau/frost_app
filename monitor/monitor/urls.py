from django.conf.urls import url

from . import views


urlpatterns = [
               url(r'^$', views.index, name='index'),
               url(r'^admin/$', views.admin, name='admin'),
               url(r'^login/$', views.login_admin, name='login_admin'),
               ]
