# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-02 17:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('monitor', '0003_sitestatus'),
    ]

    operations = [
        migrations.CreateModel(
            name='TargetList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('target_list', models.TextField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
