# FRoST Monitor System
###Web application for displaying current information concerning the operation of the Flagstaff Robotic Survey Telescope **(FRoST)**
* Version: `1.0`
* Authors: `Gage Cotrell` `Alexander Sears` `Justin Kincaid` `Chris French`
* Sponsors: `Dr. Mommert` `Dr. Trilling`
* Mentor: `Dr. Otte`

## Introduction
* Congratulations! Welcome to the wonderful world of the `FRoST monitor system`. This `README` will document the steps that must be taken to to get the application up and running locally.

## Required Software
* Python 3.7
* DJANGO 1.1

## Getting Started
* First click on the `clone` link under the actions column on the top left side of the page. This clone link can also be found next the the `HTTPS` address on the top right corner of the page.
* Next, copy this address. The address should be in the format `https://<ProfileName>@bitbucket.org/skywardnau/frost_app.git`
* Once the address is copied, open your command line and `CD` to the directory of choice for the `frost_app`
* Once in the folder of choice for the `frost_app`, use the 'git clone' command and paste the previous address directly after the command
* The format should look like `git clone https://<ProfileName>@bitbucket.org/skywardnau/frost_app.git`
* This git command should automatically download the git into your chosen directory

##Awesome
*now that you have the repo downloaded we can move onto installing and setting up the application*

The best place to find information on the *FRoST MONITOR* web application is the...
#[FRoST MONITOR WIKI](https://bitbucket.org/skywardnau/frost_app/wiki/)